<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_advertise_code".
 *
 * @property int $id
 * @property int $prod_id
 * @property string $advertiser
 * @property string $code
 * @property int $status
 *
 * @property Product $prod
 */
class ProductAdvertiseCode extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_advertise_code';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prod_id', 'advertiser', 'code'], 'required'],
            [['prod_id', 'status'], 'integer'],
            [['code'], 'string'],
            [['advertiser'], 'string', 'max' => 50],
            [['prod_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['prod_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prod_id' => 'Product Name',
            'advertiser' => 'Advertiser',
            'code' => 'Code Block',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProd()
    {
        return $this->hasOne(Product::className(), ['id' => 'prod_id']);
    }
}
