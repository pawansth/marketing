<?php

namespace common\models;

use Yii;

use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $dob
 * @property string $mobile
 * @property string $user_name
 * @property string $password
 * @property string $email
 * @property string $img
 * @property string $auth_key
 * @property string $token
 * @property int $status
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'address', 'dob', 'mobile', 'username', 'password'], 'required'],
            [['dob'], 'safe'],
            [['status'], 'integer'],
            [['name', 'address', 'user_name', 'password', 'email'], 'string', 'max' => 50],
            [['mobile'], 'string', 'max' => 20],
            [['img'], 'string', 'max' => 100],
            [['auth_key', 'token'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'address' => 'Address',
            'dob' => 'Dob',
            'mobile' => 'Mobile',
            'username' => 'User Name',
            'password' => 'Password',
            'email' => 'Email',
            'img' => 'Img',
            'auth_key' => 'Auth Key',
            'token' => 'Token',
            'status' => 'Status',
        ];
    }

    public function getAuthKey() {
        return $this->auth_key;
    }

    public function getId() {
        return $this->id;
    }

    public function validateAuthKey($authKey) {
        
    }

    public static function findIdentity($id): IdentityInterface {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null): IdentityInterface {
        return $this->token;
    }
    
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
       
        return self::find()->where(['username'=>$username])->one();
    }
    
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {

        return $this->password === $password;
    }

}
