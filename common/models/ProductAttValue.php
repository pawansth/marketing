<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_att_value".
 *
 * @property int $id
 * @property int $prod_att_id
 * @property string $value
 * @property int $prod_id
 * @property int $status
 *
 * @property ProductAttribute $prodAtt
 * @property Product $prod
 */
class ProductAttValue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_att_value';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prod_att_id', 'value', 'prod_id'], 'required'],
            [['prod_att_id', 'prod_id', 'status'], 'integer'],
            [['value'], 'string', 'max' => 150],
            [['prod_att_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductAttribute::className(), 'targetAttribute' => ['prod_att_id' => 'id']],
            [['prod_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['prod_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prod_att_id' => 'Product Specification / Header',
            'value' => 'Value',
            'prod_id' => 'Product Name',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdAtt()
    {
        return $this->hasOne(ProductAttribute::className(), ['id' => 'prod_att_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProd()
    {
        return $this->hasOne(Product::className(), ['id' => 'prod_id']);
    }
}
