

<div class="aside white-background" style="background: #FFFFFF; padding-bottom: 10px;">
    <h3 class="aside-title" style="padding-left: 10px; padding-right: 10px; padding-bottom: 3px; padding-top: 3px; margin-bottom: 15px;">
        Price Rang 
        <span class="fa fa-minus" onclick="
                if ($(this).hasClass('fa-minus')) {
                    $('.price-range-block').hide();
                    $(this).removeClass('fa-minus');
                    $(this).addClass('fa-plus');
                } else {
                    $('.price-range-block').show();
                    $(this).removeClass('fa-plus');
                    $(this).addClass('fa-minus');
                }" style="
              color: #C7C4C4;
              float: right;
              text-align: center;
              padding-top: 4px;
              "></span>
    </h3>
    <div class="price-range-block"  style="padding-left: 10%; padding-right: 10%;">
        <div class="row" style="padding-left: 10px; padding-right: 10px;">
            <div id="slider-range" class="price-filter-range" name="price_range"></div>
        </div>
        <div class="row" style="border-bottom: 1px dotted; padding-bottom: 5px;">
            <span id="min_price_view" style="float: left">0</span>
            <span id="max_price_view" style="float: right">50000 +</span>
        </div>

        <div class="row" style="padding-top: 10px;">

            <form role="form" action="#" method="get">
                <div class="input-group input-group-sm mb-3">
                    <input type="text" id="min_price" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" oninput="validity.valid||(value='0');">
                    <div class="input-group-addon">
                        <span class="input-group-text" id="inputGroup-sizing-sm">To</span>
                    </div>
                    <input type="text" id="max_price" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" oninput="validity.valid||(value='50000');">
                    <div class="input-group-addon">
                        <button type="submit" class="fa fa-search price_search" style="
                                border: none;
                                background: none;
                                padding: 0px;
                                margin: 0xp;
                                font-size: 14px;
                                "></button>
                    </div>
                </div>
            </form>

        </div>

    </div>

</div>
<?php if (!empty($brand)) { ?>
    <div class="aside white-background" style="background: #FFFFFF; padding-bottom: 10px;">
        <h3 class="aside-title" style="
            padding-left: 10px;
            padding-right: 10px;
            padding-bottom: 3px;
            padding-top: 3px;
            margin-bottom: 15px;
            ">
            Brand 
            <span class="fa fa-minus" onclick="
                    if ($(this).hasClass('fa-minus')) {
                        $('.brand-block').hide();
                        $(this).removeClass('fa-minus');
                        $(this).addClass('fa-plus');
                    } else {
                        $('.brand-block').show();
                        $(this).removeClass('fa-plus');
                        $(this).addClass('fa-minus');
                    }" style="
                  color: #C7C4C4;
                  float: right;
                  text-align: center;
                  padding-top: 4px;
                  "></span>
        </h3>

        <div class="brand-block"  style="padding-left: 10%; padding-right: 10%; height: 25%;">
            <div class="search-brand" style="display: block;">
                <input type="text" autocomplete="off" class="form-control" 
                       onkeyup="
                               var filter = $(this).val().toLowerCase();
                               $.each($('ul.brand-list >li.brand-li'), function (e) {
                                   if ($(this).find('.brand-name').attr('data-text').toLowerCase().indexOf(filter) > -1) {
                                       $(this).css('display', 'block');
                                   } else {
                                       $(this).css('display', 'none');
                                   }
                               });
                                                  " style="
                                                  background-image: url('css/search-icon-png-18.png');
                                                  background-position: 8px 8px;
                                                  background-repeat: no-repeat;
                                                  padding-left: 28px;
                                                  "/>
            </div>
            <div style="overflow-y: auto; width: 100%; height: 100%; padding-top: 10px;">
                <ul class="brand-list">
                <?php
                foreach ($brand as $value) {
                    ?>
                    <li class="brand-li" style="display: block">
                        <label class="checkbox-item" style="display: block">
                            <input autocomplete="off" type="checkbox" name="brand[]"
                                   value="<?= $value->id ?>" class="brand-name" data-text="<?= $value->name ?>" />
                            <span><?= $value->name ?></span>
                        </label>
                    </li>
                    <?php
                }
                ?>
            </ul>
            </div>
        </div>
    </div>
<?php } ?>

<?php
if (!empty($productSpecification)) {
    $arrange = array();
    foreach ($productSpecification as $value) {
        if (empty($arrange) || !array_key_exists($value['name'], $arrange)) {
            $arrange[$value['name']] = [$value];
        } else {
            if (!in_array($value['value'], array_column($arrange[$value['name']], 'value'))) {
                array_push($arrange[$value['name']], $value);
            }
        }
    }
    ?>
    <div class="aside" style="background: #FFFFFF; padding-bottom: 10px;">
        <h3 class="aside-title" style="
            padding-left: 10px;
            padding-right: 10px;
            padding-bottom: 3px;
            padding-top: 3px;
            margin-bottom: 15px;
            ">
            Specification
            <span class="fa fa-minus" onclick="
                    if ($(this).hasClass('fa-minus')) {
                        $('.specificaiton-block').hide();
                        $(this).removeClass('fa-minus');
                        $(this).addClass('fa-plus');
                    } else {
                        $('.specificaiton-block').show();
                        $(this).removeClass('fa-plus');
                        $(this).addClass('fa-minus');
                    }" style="
                  color: #C7C4C4;
                  float: right;
                  text-align: center;
                  padding-top: 4px;
                  "></span>
        </h3>

        <div class="specificaiton-block"  style="padding-left: 10%; padding-right: 10%; height: 25%; overflow-y: auto;">
            <?php
            $i = 0;
            foreach ($arrange as $key) {
                ?>
                <div class="specification-sub-block-parent<?= $i; ?>" style="width: 100%; border-bottom: 1px solid; margin-bottom: 15px; padding-bottom: 10px;">
                    <div style="width: 100%; border-bottom: 1px solid; margin-bottom: 10px;">
                        <h5>
                            <?= $key[0]['name'] ?>
                            <span class="fa fa-minus" onclick="
                                    if ($(this).hasClass('fa-minus')) {
                                        $('.specificaiton-sub-block<?= $i; ?>').hide();
                                        $('.specification-sub-block-parent<?= $i; ?>').css('border-bottom', '');
                                        $(this).removeClass('fa-minus');
                                        $(this).addClass('fa-plus');
                                    } else {
                                        $('.specificaiton-sub-block<?= $i; ?>').show();
                                        $('.specification-sub-block-parent<?= $i; ?>').css('border-bottom', '1px solid');
                                        $(this).removeClass('fa-plus');
                                        $(this).addClass('fa-minus');
                                    }" style="
                                  color: #C7C4C4;
                                  float: right;
                                  text-align: center;
                                  padding-top: 1px;
                                  font-size: 14px;
                                  "></span>
                        </h5>
                    </div>
                    <div class="specificaiton-sub-block<?= $i ?>" style="width: 100%;">
                        <ul class="att_ul">
                            <?php
                            foreach ($key as $data) {
                                ?>
                            <li class="att_li">
                                        <label class="att-label">
                                            <input type="checkbox" class="prod_att" name="spec[]" 
                                                   att_id="<?= $data['id']?>" att_val="<?= $data['value']?>"
                                                   value="<?= $data['value'] ?>" />
                                            <span><?= $data['value'] ?></span> 
                                        </label>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <?php
                $i++;
            }
            ?>
        </div>
    </div>
    <?php
}
?>
