<?php if (empty($products)) { ?>
    <div class="jumbotron-holder">
        <div class="jumbotron" style="background: #F0795F; color: white;">
            No product found....
        </div>
    </div>
<?php } else {
    ?>
    <?php foreach ($products as $product) { ?>
        <a href="?r=product/product-detail&id=<?= $product['id'] ?>&brandId=<?= $product['brand_id'] ?>">
            <div class="product-item" prod_id="<?= $product['id'] ?>">
                <div class="product-img" style="
                     width: 100%; height: 75%; padding: 10px; position: relative;">
                     <?php
                     $date_diff = intval(date_diff(date_create($product['release_date']), date_create(date('Y-m-d')))->format('%R%a'));
                     if ($date_diff > 0 && $date_diff < 90) {
                         ?>
                        <div class="product-label" style="
                             z-index: 9999; position: absolute; background: #ff1744; color: white; font-weight: bold; padding: 5px;">
                            <span class="sale">New</span>
                        </div>
                    <?php } elseif ($date_diff < 0) { ?>
                        <div class="product-label" style="
                             z-index: 9999; position: absolute; background: #ff1744; color: white; font-weight: bold; padding: 5px;">
                            <span class="sale">Upcoming</span>
                        </div>
                    <?php } ?>  
                    <img src="uploads/<?= $product['path'] ?>" style="
                         width: 98%;
                         height: 100%;
                         display: block;
                         margin-left: auto;
                         margin-right: auto;
                         "/>
                </div>
                <div class="product-detail" style="width: 100%; height: 30%">
                    <div class="product-name">
                        <span>
                            <?= $product['name']; ?>
                        </span><br>
                        <span><span class="fa fa-dollar"></span> <?= $product['price'] ?></span>
                    </div>
                    <div class="product-star" style="width: 50%; margin-left: auto; margin-right: auto;">
                        <?php
                        if ($product['rating'] == 1) {
                            ?>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <?php
                        } elseif ($product['rating'] > 1 && $product['rating'] < 2) {
                            ?>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star-half-full checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <?php
                        } elseif ($product['rating'] == 2) {
                            ?>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <?php
                        } elseif ($product['rating'] > 2 && $product['rating'] < 3) {
                            ?>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star-half-full checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <?php
                        } elseif ($product['rating'] == 3) {
                            ?>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <?php
                        } elseif ($product['rating'] > 3 && $product['rating'] < 4) {
                            ?>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star-half-full checked"></span>
                            <span class="fa fa-star"></span>
                            <?php
                        } elseif ($product['rating'] == 4) {
                            ?>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <?php
                        } elseif (($product['rating'] > 4) && ($product['rating'] < 5)) {
                            ?>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star-half-full checked"></span>
                            <?php
                        } elseif ($product['rating'] == 5) {
                            ?>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <?php
                        } else {
                            ?>
                            <span class="fa fa-star-o"></span>
                            <span class="fa fa-star-o"></span>
                            <span class="fa fa-star-o"></span>
                            <span class="fa fa-star-o"></span>
                            <span class="fa fa-star-o"></span>
                            <?php
                        }
                        ?>
                    </div>

                    <div class="compare-div">
                        <div class="compare-sub-div">
                            <label class="compare-label">
                                <input type="checkbox" class="compare" 
                                       prod_id="<?= $product['id'] ?>" 
                                       prod_img="uploads/<?= $product['path'] ?>" 
                                       prod_name="<?= $product['name'] ?>">
                                <span> Compare</span>
                            </label>
                        </div>
                    </div>

                </div>
            </div>
        </a> 
    <?php }
    ?>
    <?php
}?>