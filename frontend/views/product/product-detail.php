<?php
$this->registerCss(
        ".container {"
        . "min-height: 100vh;"
        . "margin-top: 20px;"
        . "background: #FFFFFF;"
        . "}"
        . ".product_image {"
        . "width: 40%;"
        . "height: 50%;"
        . "margin-top: 20px;"
        . "margin-bottom: 20px;"
        . "}"
        . ".adv-main-div {"
        . "margin-top: 20px;"
        . "margin-bottom: 20px;"
        . "}"
        . ".checked {"
        . "color: orange;"
        . "}"
);

//$this->registerJs(
//        "$(function() { alert('hi');"
//        . "$('[data-spzoom]').spzoom();"
//        . "});"
//);

$this->title = $productDetail[0]['name'];
$parentTitle = \common\models\Categories::find()
                ->where([
                    'id' => $productDetail[0]['sub_cat_id']
                ])
                ->one()
        ->name;
$this->params['breadcrumbs'][] = [
    'label' => $parentTitle,
    'url' => [
        'product/product-list', 'id' => $productDetail[0]['sub_cat_id']
    ]
];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container" style="background: #FFFFFF;">
    <div style="padding: 10px; border-bottom: 1px solid #888888;">
        <span style="
              font-size: 16px;
              font-weight: bold;
              "><?= $this->title ?></span>
    </div>
    <div class="col-md-5 product_image">
        <a href="uploads/<?= $productDetail[0]['path'] ?>" data-spzoom>
            <img class="product-image" src="uploads/<?= $productDetail[0]['path'] ?>" style="
                 width: 95%;
                 height: 100%;
                 ">
        </a>
    </div>
    <?php
//        $this->registerJs(
//                "$('.product_image').hover(function() {"
//                . "$(this).removeClass('col-md-5');"
//                . "$(this).addClass('col-md-12');"
//                . "$('.adv-main-div').css('display', 'none');"
//                . "},"
//                . " function() {"
//                . "$(this).removeClass('col-md-12');"
//                . "$(this).addClass('col-md-5');"
//                . "}"
//                . ");"
//                );
    ?>
    <div class="col-md-7 adv-main-div">
        <?php foreach ($prodAdvCode as $code) { ?>
            <div class="<?= $code['advertiser'] ?>">
                <?= $code['code'] ?>
            </div>
        <?php }
        ?>
    </div>

    <?php
    $arrange = array();
    foreach ($productSpecification as $value) {
        if (empty($arrange) || !array_key_exists($value['name'], $arrange)) {
            $arrange[$value['name']] = [$value];
        } else {
            if (!in_array($value['value'], array_column($arrange[$value['name']], 'value'))) {
                array_push($arrange[$value['name']], $value);
            }
        }
    }
    $i = 0;
    foreach ($arrange as $val) {
        $parent = \common\models\ProductAttribute::find()
                        ->where(['id' => $val[0]['parent']])
                        ->one()
                ->name;
        ?>
        <div class="<?= $parent ?>" style="float: left; padding-right: 30px; padding-left: 10px;
             <?php if (($i < (sizeof($arrange) - 1))) { ?>border-right: 1px solid #888888;<?php } ?>">
            <div style="border-bottom: 3px solid greenyellow; padding: 5px;">
                <span><?= $val[0]['name'] ?></span>
            </div>
            <?php foreach ($val as $data) { ?>
                <div>
                    <div style="padding: 5px;">
                        <span>
                            <?= $data['value'] ?>
                        </span>
                    </div>
                </div>
            <?php }
            ?>
        </div>
        <?php
        $i++;
    }
    ?>


    <div class="section">
        <!-- container -->
        <div class="container-fluid">

            <!-- row for rating and review -->
            <div class="container-fluid">
                <div class="row review_rating" style="
                     width: 100%;
                     border-top: 1px solid #B8B1B0;
                     border-bottom: 1px solid #B8B1B0;
                     height: 30vh;">
                    <div class="col-md-4 overall_rating" style="
                         width: 32%;
                         height: 100%;
                         border-right: 1px solid #B8B1B0;
                         text-align: center;
                         ">
                        <div style="margin-top: 30px; color: #645C5B; font-size: 16px; font-weight: bold; color: #A39F9E;">
                            <span>OVERALL RATING</span>
                        </div>
                        <?php
                        $rate = Yii::$app->db->createCommand('SELECT SUM(rating)/COUNT(rating) AS rating, COUNT(rating) AS total FROM product_rating WHERE prod_id= ' . $productDetail[0]['id'] . ' GROUP BY prod_id')
                                ->queryAll();
                        ?>
                        <div>
                            <span style="
                                  font-size: 50px;
                                  font-weight: bold;
                                  margin: 0;
                                  padding: 0;
                                  ">
                                      <?= !empty($rate) ? number_format((float) $rate[0]['rating'], 2, '.', '') : 0; ?>
                            </span>
                            <span style="
                                  font-size: 20px;
                                  color: #A39F9E;
                                  margin: 0;
                                  padding: 0;
                                  ">
                                /5
                            </span>
                            <span style="
                                  display: block;
                                  font-size: 14px;
                                  font-weight: bold;
                                  color: #A39F9E;
                                  ">
                                BASED ON <?= !empty($rate) ? $rate[0]['total'] : 0; ?> RATING(S)
                            </span>
                        </div>
                    </div>
                    <div class="col-md-4 graph" style="
                         width: 32%;
                         height: 100%;
                         border-right: 1px solid #B8B1B0;
                         padding-top: 30px;
                         color: #B8B1B0;
                         ">
                             <?php
                             $totalRateCount = Yii::$app->db->createCommand('SELECT'
                                             . ' COUNT(rating) AS total FROM product_rating'
                                             . ' WHERE prod_id= ' . $productDetail[0]['id']
                                             . ' GROUP BY prod_id')
                                     ->queryAll();
                             $rate5 = Yii::$app->db->createCommand('SELECT'
                                             . ' COUNT(rating) AS total FROM product_rating'
                                             . ' WHERE prod_id= ' . $productDetail[0]['id']
                                             . ' AND rating = 5 GROUP BY prod_id')
                                     ->queryAll();
                             ?>
                        <div style="width: 100%; height: 25px;">
                            <div style="display: inline-block; width: 20%; float: left;">
                                <span style="">5 stars</span>
                            </div>
                            <div style="display: inline-block; width: 60%; float: left;">
                                <div style="
                                     border: 1px solid #D6785E;
                                     width: 100%;
                                     height: 20px;
                                     ">
                                    <div style="
                                         height: 100%;
                                         width: <?= !empty($rate5) ? intval(($rate5[0]['total'] / $totalRateCount[0]['total'])*100) : 0; ?>%;
                                         background: red;
                                         "></div>
                                </div>
                            </div>
                            <div style="display: inline-block; width: 20%; float: left;">
                                <span style="padding-left: 5px;"><?= !empty($rate5) ? $rate5[0]['total'] : 0; ?></span>
                            </div>

                        </div>

                        <?php
                        $rate4 = Yii::$app->db->createCommand('SELECT'
                                        . ' COUNT(rating) AS total FROM product_rating'
                                        . ' WHERE prod_id= ' . $productDetail[0]['id']
                                        . ' AND rating = 4 GROUP BY prod_id')
                                ->queryAll();
                        ?>
                        <div style="width: 100%; height: 25px;">
                            <div style="display: inline-block; width: 20%; float: left;">
                                <span style="">4 stars</span>
                            </div>
                            <div style="display: inline-block; width: 60%; float: left;">
                                <div style="
                                     border: 1px solid #D6785E;
                                     width: 100%;
                                     height: 20px;
                                     ">
                                    <div style="
                                         height: 100%;
                                         width: <?= !empty($rate4) ? intval(($rate4[0]['total'] / $totalRateCount[0]['total'])*100) : 0; ?>%;
                                         background: red;
                                         "></div>
                                </div>
                            </div>
                            <div style="display: inline-block; width: 20%; float: left;">
                                <span style="padding-left: 5px;"><?= !empty($rate4) ? $rate4[0]['total'] : 0; ?></span>
                            </div>

                        </div>

                        <?php
                        $rate3 = Yii::$app->db->createCommand('SELECT'
                                        . ' COUNT(rating) AS total FROM product_rating'
                                        . ' WHERE prod_id= ' . $productDetail[0]['id']
                                        . ' AND rating = 3 GROUP BY prod_id')
                                ->queryAll();
                        ?>
                        <div style="width: 100%; height: 25px;">
                            <div style="display: inline-block; width: 20%; float: left;">
                                <span style="">3 stars</span>
                            </div>
                            <div style="display: inline-block; width: 60%; float: left;">
                                <div style="
                                     border: 1px solid #D6785E;
                                     width: 100%;
                                     height: 20px;
                                     ">
                                    <div style="
                                         height: 100%;
                                         width: <?= !empty($rate3) ? intval(($rate3[0]['total'] / $totalRateCount[0]['total'])*100) : 0; ?>%;
                                         background: red;
                                         "></div>
                                </div>
                            </div>
                            <div style="display: inline-block; width: 20%; float: left;">
                                <span style="padding-left: 5px;"><?= !empty($rate3) ? $rate3[0]['total'] : 0; ?></span>
                            </div>

                        </div>

                        <?php
                        $rate2 = Yii::$app->db->createCommand('SELECT'
                                        . ' COUNT(rating) AS total FROM product_rating'
                                        . ' WHERE prod_id= ' . $productDetail[0]['id']
                                        . ' AND rating = 2 GROUP BY prod_id')
                                ->queryAll();
                        ?>
                        <div style="width: 100%; height: 25px;">
                            <div style="display: inline-block; width: 20%; float: left;">
                                <span style="">2 stars</span>
                            </div>
                            <div style="display: inline-block; width: 60%; float: left;">
                                <div style="
                                     border: 1px solid #D6785E;
                                     width: 100%;
                                     height: 20px;
                                     ">
                                    <div style="
                                         height: 100%;
                                         width: <?= !empty($rate2) ? intval(($rate2[0]['total'] / $totalRateCount[0]['total'])*100) : 0; ?>%;
                                         background: red;
                                         "></div>
                                </div>
                            </div>
                            <div style="display: inline-block; width: 20%; float: left;">
                                <span style="padding-left: 5px;"><?= !empty($rate2) ? $rate2[0]['total'] : 0; ?></span>
                            </div>

                        </div>


                        <?php
                        $rate1 = Yii::$app->db->createCommand('SELECT'
                                        . ' COUNT(rating) AS total FROM product_rating'
                                        . ' WHERE prod_id= ' . $productDetail[0]['id']
                                        . ' AND rating = 1 GROUP BY prod_id')
                                ->queryAll();
                        ?>
                        <div style="width: 100%; height: 25px;">
                            <div style="display: inline-block; width: 20%; float: left;">
                                <span style="">1 stars</span>
                            </div>
                            <div style="display: inline-block; width: 60%; float: left;">
                                <div style="
                                     border: 1px solid #D6785E;
                                     width: 100%;
                                     height: 20px;
                                     ">
                                    <div style="
                                         height: 100%;
                                         width: <?= !empty($rate1) ? intval(($rate1[0]['total'] / $totalRateCount[0]['total'])*100) : 0; ?>%;
                                         background: red;
                                         "></div>
                                </div>
                            </div>
                            <div style="display: inline-block; width: 20%; float: left;">
                                <span style="padding-left: 5px;"><?= !empty($rate1) ? $rate1[0]['total'] : 0; ?></span>
                            </div>

                        </div>


                    </div>
                    <div class="col-md-4 btns" style="
                         width: 32%;
                         height: 100%;
                         text-align: center;
                         ">
                        <div style="margin-top: 30px; color: #645C5B; font-size: 16px; font-weight: bold; color: #A39F9E;">
                            <span> SHARE YOUR THOUGHTS </span>
                        </div>

                        <a class="btn review-btn" href="?r=product/product-review&id=<?= $productDetail[0]['id'] ?>">WRITE A REVIEW</a>
                        <a class="btn review-btn" href="?r=product/product-rate&id=<?= $productDetail[0]['id'] ?>">Rate A PRODUCT</a>
                        <?php
                        $this->registerCss(
                                ".review-btn {"
                                . "border: 1px solid #F0795F;"
                                . "margin: 10px;"
                                . "}"
                                . ".review-btn:hover {"
                                . "background: #F0795F;"
                                . "}"
                        );
                        ?>
                    </div>
                </div>

                <!-- Review block -->

                <div class="row" style="width: 100%; height: 30vh;">

                    <?php
                    $review = \common\models\ProductReview::find()
                            ->where([
                                'prod_id' => $productDetail[0]['id']
                            ])
                            ->all();
                    ?>
                    <div <?php if (!empty($review)) { ?>
                            style="width: 100%;
                            height: 50vh;
                            overflow-x: auto;
                            "
                        <?php } ?>>
                        <div class="section-title" style="
                             width: 100%;
                             padding-left: 10px;
                             padding-bottom: 5px;
                             font-size: 18px;
                             font-weight: bold;">
                            Reviews
                        </div>
                            <?php
                            if (!empty($review)) {
                                foreach ($review as $key => $value) {
                                    ?>
                        <div style="
                             width: 90%;
                             margin-left: auto;
                             margin-right: auto;
                             border-bottom: 1px solid #9D9391;
                             padding: 5px;">
                            <div style="padding: 5px;">
                                        <span style="float: left;"><?= $value['email'] ?></span>
                                        <span style="float: right;"><?= $value['review_date'] ?></span> <br>
                                    </div>
                            <div style="height: 80%; width: 70%; margin-left: auto; margin-right: auto; padding-bottom: 10px;">
                                        <p><?= $value['review']?></p>
                                    </div>
                                </div>
                            <?php
                            }
                        }
                        ?>
                    </div>

                </div>

            </div>
            <!-- row -->
            <div class="row">
                <!-- section title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h2 class="title">Related Products</h2>
                    </div>
                </div>
                <!-- section title -->
                <!-- section-title -->
                <div class="col-md-12">
                    <div class="pull-right">
                        <div class="product-slick-dots-6 custom-dots"></div>
                    </div>
                </div>
                <!-- /section-title -->
                <!-- Product Slick -->
                <div class="col-md-12">
                    <div class="row">
                        <div id="product-slick-6" class="product-slick">
                            <?php
                            foreach ($relatedProduct as $item) {
                                ?>
                                <!-- Product Single -->
                                <div class="product product-single">
                                    <div class="product-thumb">
                                        <?php
                                        $date_diff = intval(date_diff(date_create($item['release_date']), date_create(date('Y-m-d')))->format('%R%a'));
                                        if ($date_diff > 0 && $date_diff < 90) {
                                            ?>
                                            <div class="product-label">
                                                <span class="sale">New</span>
                                            </div>
    <?php } elseif ($date_diff < 0) { ?>
                                            <div class="product-label">
                                                <span class="sale">Upcoming</span>
                                            </div>
    <?php } ?>

                                        <a href="?r=product/product-detail&id=<?= $item['id'] ?>&brandId=<?= $item['brand_id'] ?>">
                                            <button class="main-btn quick-view"> 
                                                <i class="fa fa-search-plus"></i>
                                                Quick view
                                            </button>
                                        </a>
                                        <img src="uploads/<?= $item['path'] ?>" data-lazy="uploads/<?= $item['path'] ?>" alt="">
                                    </div>
                                    <div class="product-body">
                                        <h2 style="
                                            text-align: center;
                                            font-size: 18px;
                                            font-weight: bold;
                                            ">
                                            <a href="?r=product/product-detail&id=<?= $item['id'] ?>&brandId=<?= $item['brand_id'] ?>">
    <?= $item['name'] ?>
                                            </a>
                                        </h2>
                                        <h3 style="
                                            text-align: center;
                                            font-size: 16px;
                                            font-weight: bold;
                                            ">
                                            <span class="fa fa-dollar"></span>
                                            <span> <?= $item['price'] ?></span>
                                        </h3>
                                        <div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
                                            <?php
                                            if ($item['rating'] == 1) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] > 1 && $item['rating'] < 2) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star-half-full checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] == 2) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] > 2 && $item['rating'] < 3) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star-half-full checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] == 3) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] > 3 && $item['rating'] < 4) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star-half-full checked"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] == 4) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif (($item['rating'] > 4) && ($item['rating'] < 5)) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star-half-full checked"></span>
                                                <?php
                                            } elseif ($item['rating'] == 5) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <?php
                                            } else {
                                                ?>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <?php
                                            }
                                            ?>
                                        </div>

                                    </div>
                                </div>
                                <!-- /Product Single --> 
<?php } ?>                   
                        </div>
                    </div>
                </div>
                <!-- /Product Slick -->


            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /section -->

</div>