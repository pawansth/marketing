<?php
/* @var $this yii\web\View */

$this->title = Yii::$app->name;
?>
<div class="site-index" style="background: #FFFFFF;">
    <div class="row" style="margin-top: 20px; padding-left: 20px; padding-right: 20px; padding-top: 10px; padding-bottom: 10px;">
        <div class="col-md-10" style="">
            <!-- home slick -->
            <div id="home-slick">
                <?php
                $mainslider = common\models\Slider::find()->all();
                foreach ($mainslider as $slider) {
                    ?>
                    <!-- banner -->
                    <div class="banner banner-1" style="height: 70vh;">
                        <img src="uploads/<?= $slider->path ?>" alt="" class="slider-img">
                        <div class="banner-caption text-center">
                            <h1><?= $slider->title ?></h1>
                            <a class="primary-btn" href="<?= $slider->link ?>">GO</a>
                        </div>
                    </div>
                    <!-- /banner -->
                <?php } ?>

            </div>
            <!-- /home slick -->
        </div>
        <div class="col-md-2"></div>
    </div>

    <!-- Home page content -->

    <div>

        <?php
        $categories = common\provider\CategoriesProvider::categoriesList();
        foreach ($categories as $cat) {
            if ((sizeof($cat) <= 7)) {
                createContent($cat['id'], $cat['name']);
            } else {
                recursives($cat);
            }
        }

        function recursives($data) {
            if ((sizeof($data) <= 7) && (!($data['parent'] == 0))) {
                createContent($data['id'], $data['name']);
            }
            for ($i = 0; $i < sizeof($data) - 7; $i++) {
                recursives($data[$i]['node']);
            }
        }
        
        function createContent($id, $name) { 
            $query = 'SELECT product.*, table1.rating, table2.path FROM product'
                . ' LEFT JOIN (SELECT SUM(rating) / COUNT(rating) AS rating,'
                . ' prod_id  FROM product_rating GROUP BY (prod_id)) table1'
                . ' ON product.id = table1.prod_id LEFT JOIN (SELECT * FROM'
                . ' product_img GROUP BY (prod_id)) table2 ON table2.prod_id'
                . ' = product.id'
                . ' WHERE product.sub_cat_id = :sub_id';
        $relatedProduct = \Yii::$app->db->createCommand($query)
                ->bindValue(':sub_id', $id)
                ->queryAll();
        if(empty($relatedProduct)) {            return;}
            ?>
        <div class="row" style="padding-right: 20px; padding-left: 20px;">
                <!-- section title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h2 class="title"><?= $name?></h2>
                        <a 
                            class="btn btn-primary" 
                            style="background: #F0795F;
                            border: 1px solid #F0795F;
                            float: right;"
                            href="<?= Yii::$app->homeUrl ?>?r=product/product-list&id=<?= $id ?>">
                            View All 
                        </a>
                    </div>
                </div>
                <!-- section title -->
                <!-- section-title -->
                <div class="col-md-12">
                    <div class="pull-right">
                        <div class="product-slick-dots-6 custom-dots"></div>
                    </div>
                </div>
                <!-- /section-title -->
                <!-- Product Slick -->
                <div class="col-md-12">
                    <div class="row">
                        <div id="product-slick-6" class="product-slick">
                            <?php
                            foreach ($relatedProduct as $item) {
                                ?>
                                <!-- Product Single -->
                                <div class="product product-single">
                                    <div class="product-thumb">
                                        <?php
                                        $date_diff = intval(date_diff(date_create($item['release_date']), date_create(date('Y-m-d')))->format('%R%a'));
                                        if ($date_diff > 0 && $date_diff < 90) {
                                            ?>
                                            <div class="product-label">
                                                <span class="sale">New</span>
                                            </div>
    <?php } elseif ($date_diff < 0) { ?>
                                            <div class="product-label">
                                                <span class="sale">Upcoming</span>
                                            </div>
    <?php } ?>

                                        <a href="?r=product/product-detail&id=<?= $item['id'] ?>&brandId=<?= $item['brand_id'] ?>">
                                            <button class="main-btn quick-view"> 
                                                <i class="fa fa-search-plus"></i>
                                                Quick view
                                            </button>
                                        </a>
                                        <img src="uploads/<?= $item['path'] ?>" data-lazy="uploads/<?= $item['path'] ?>" alt="">
                                    </div>
                                    <div class="product-body">
                                        <h2 style="
                                            text-align: center;
                                            font-size: 18px;
                                            font-weight: bold;
                                            ">
                                            <a href="?r=product/product-detail&id=<?= $item['id'] ?>&brandId=<?= $item['brand_id'] ?>">
    <?= $item['name'] ?>
                                            </a>
                                        </h2>
                                        <h3 style="
                                            text-align: center;
                                            font-size: 16px;
                                            font-weight: bold;
                                            ">
                                            <span class="fa fa-dollar"></span>
                                            <span> <?= $item['price'] ?></span>
                                        </h3>
                                        <div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
                                            <?php
                                            if ($item['rating'] == 1) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] > 1 && $item['rating'] < 2) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star-half-full checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] == 2) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] > 2 && $item['rating'] < 3) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star-half-full checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] == 3) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] > 3 && $item['rating'] < 4) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star-half-full checked"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif ($item['rating'] == 4) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <?php
                                            } elseif (($item['rating'] > 4) && ($item['rating'] < 5)) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star-half-full checked"></span>
                                                <?php
                                            } elseif ($item['rating'] == 5) {
                                                ?>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <?php
                                            } else {
                                                ?>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <?php
                                            }
                                            ?>
                                        </div>

                                    </div>
                                </div>
                                <!-- /Product Single --> 
<?php } ?>                   
                        </div>
                    </div>
                </div>
                <!-- /Product Slick -->


            </div>
            <?php }
        ?>

    </div>

</div>
