$(document).ready(function () {

    $(document).find('[data-spzoom]').spzoom();

    $(document).on('click',
            '.price_search,' +
            ' ul.brand-list>li.brand-li>label.checkbox-item, ' +
            'ul.att_ul>li.att_li>label.att-label',
            function (e) {
                e.preventDefault();
                if ($(this).find('.brand-name:checked').length > 0) {
                    // set corrosponding check box check option to false
                    $(this).find('.brand-name').prop('checked', false);
                } else {
                    // set corrosponding check box check option to true
                    $(this).find('.brand-name').prop('checked', true);
                }

                if ($(this).find('.prod_att:checked').length > 0) {
                    // set corrosponding check box check option to false
                    $(this).find('.prod_att').prop('checked', false);
                } else {
                    // set corrosponding check box check option to true
                    $(this).find('.prod_att').prop('checked', true);
                }

                var brand = [];

                $.each($(document).find('.brand-name:checked'), function () {
                    if ($(this).length > 0) {
                        brand.push($(this).val());
                    }
                });

                var prod_att = [];
                $.each($(document).find('.prod_att:checked'), function () {
                    if ($(this).length > 0) {
                        var value = {
                            'att_id': $(this).attr('att_id'),
                            'att_val': $(this).attr('att_val')
                        };
                        prod_att.push(value);
                    }
                });

                var data = {
                    'cat_id': $('#cat_id').val(),
                    'sub_cat_id': $('#sub_cat_id').val(),
                    'min_price': $('#min_price').val(),
                    'max_price': $('#max_price').val(),
                    'brand': brand,
                    'prod_att': prod_att,
                    'end_id': 0
                };

                $.post('?r=product/product-list-search', data, function (data) {
                    $('.product-list-content').html(data);
                });




            });




    var compareList = [];

    $.each($('div.compare-div>div.compare-sub-div>label.compare-label'), function () {
        if ($(this).find('.compare:checked').length > 0) {
            var prod_info = {
                'prod_id': $(this).find('.compare').attr('prod_id'),
                'prod_img': $(this).find('.compare').attr('prod_img'),
                'prod_name': $(this).find('.compare').attr('prod_name'),
                'obj': $(this)
            }
            compareList.push(prod_info);
            createCompierList();
        }
    });


    $(document).on('click', 'div.compare-div>div.compare-sub-div>label.compare-label', function (e) {
        e.preventDefault();
        if ($(this).find('.compare:checked').length > 0) {
            removeItem($(this).find('.compare').attr('prod_id'));
        } else {
            if (compareList.length == 4) {
                $('div.show_compare_main').css('display', 'block');
                return false;
            }
            $(this).find('.compare').prop('checked', true);
            var prod_info = {
                'prod_id': $(this).find('.compare').attr('prod_id'),
                'prod_img': $(this).find('.compare').attr('prod_img'),
                'prod_name': $(this).find('.compare').attr('prod_name'),
                'obj': $(this)
            }
            compareList.push(prod_info);
            createCompierList();
        }
    });

    $(document).on('click', 'div.hide-compare', function (e) {
        e.preventDefault();
        $('div.show_compare_main').css('display', 'none');
    });


    function createCompierList() {
        $('div.show_compare_main>div.show_compare_sub_main').empty();
        if (compareList.length == 0) {
            $('div.show_compare_main').css('display', 'none');
        } else {
            $('div.show_compare_main').css('display', 'block');
        }
        for (var i = 0; i < 4; i++) {
            if (i < compareList.length) {
                var htmCode = '<div class="show_compare_item" prod_id="' + compareList[i].prod_id + '">' +
                        '<div class="close_button">' +
                        '<span class="fa fa-remove"></span>' +
                        '</div>' +
                        '<div class="show_compare_item_img">' +
                        '<img src="' + compareList[i].prod_img + '" class="show_compare_item_img_item"/>' +
                        '</div>' +
                        '<div class="show_compare_item_name">' +
                        '<span class="show_compare_item_name_item">' +
                        compareList[i].prod_name +
                        '</span>' +
                        '</div>' +
                        '</div>';
                $('div.show_compare_main>div.show_compare_sub_main').append(htmCode);
            } else {
                var htmCode = '<div class="show_compare_item" prod_id="">' +
                        '<div class="show_compare_item_img">' +
                        '<input class="form-control compare-product-search" autocomplete="off" style="' +
                        'background-image: url(\'css/search-icon-png-18.png\');' +
                        'background-position: 8px 8px;' +
                        'background-repeat: no-repeat;' +
                        'padding-left: 28px;' +
                        '">' +
                        '<div class="livesearch" style="' +
                        'height: 80%; overflow-x: auto;' +
                        '"></div>' +
                        '</div>' +
                        '<div class="show_compare_item_name">' +
                        '<span class="show_compare_item_name_item">' +
                        '</span>' +
                        '</div>' +
                        '</div>';
                $('div.show_compare_main>div.show_compare_sub_main').append(htmCode);
            }
        }
        var htmButton = '<div style="display: inline-block; height: 100%; float: right; margin-right:20px;">'
                + '<div class="hide-compare" style="'
                + 'width: 22px; margin-left: 80%; margin-top: 20px; margin-bottom: 50%;'
                + ' cursor: pointer;'
                + '">'
                + '<span class="glyphicon glyphicon-remove" style="font-size: 20px; font-weight: bold;"></span>'
                + '</div>'
                + '<button class="form-control btn-compare">'
                + 'Compare <span style="color: red;">(' + compareList.length + ')</span></button>'
                + '<div class="form-control removeAll" style="cursor:pointer; margin-top: 10px; width: auto; height: auto; display: block;">'
                + '<span>Remove All</span>'
                + '</div>'
                + '</div>';
        $('div.show_compare_main>div.show_compare_sub_main').append(htmButton);

        return;
    }

    function removeItem(prod_id) {
        compareList = jQuery.grep(compareList, function (value) {
            if (value.prod_id != prod_id) {
                return value;
            } else {
                if (value.obj != 0) {
                    value.obj.find('.compare').prop('checked', false);
                }
            }
        });
        createCompierList();
    }

    function newCompareProduct(id) {
        var obj = 0;
        $.each($('div.product-item'), function () {
            if ($(this).attr('prod_id') == id) {
                obj = $(this).find('div.compare-div>div.compare-sub-div>label.compare-label');
                obj.find('.compare').prop('checked', true);
            }
        });
        return obj;
    }

    $(document).on('keyup', 'input.search-input', function (e) {
        e.preventDefault();
        if($(this).val() === '') {
            $(this).parent().find('div.livesearchproduct').empty();
            $(this).parent().find('div.livesearchproduct').css('display', 'none');
            return ;
        }
        var url = '?r=product/product-search';
        var data = {
            'key_word': $(this).val()
        };
        var obj = $(this);
        $.post(url, data, function (data) {
            obj.parent().find('div.livesearchproduct').html(data);
            obj.parent().find('div.livesearchproduct').css('display', 'block');
        });
    });
    $(document).on('click', function(e) {
        if($(e.target).hasClass('search-input') || $(e.target).hasClass('livesearchproduct')) {
            if($(document).find('div.livesearchproduct').children().length > 0) {
                $(document).find('div.livesearchproduct').css('display', 'block ');
            }
            return;
        }
        $(document).find('div.livesearchproduct').css('display', 'none');
    });
    $(document).on('keyup', 'input.compare-product-search', function (e) {
        e.preventDefault();
        var url = '?r=product/compare-product-search';
        var data = {
            'cat_id': $('input#cat_id').val(),
            'sub_cat_id': $('input#sub_cat_id').val(),
            'key_word': $(this).val()
        };
        var obj = $(this);
        $.post(url, data, function (data) {
            obj.parent().find('div.livesearch').html(data);
        });
    });

    $(document).on('click', 'li.live-search-result', function (e) {
        e.preventDefault();
        var check = 0;
        var mainObj = $(this);
        $.each(compareList, function (key, value) {
            if (mainObj.attr('prod_id') == value.prod_id) {
                check = 1;
                return false;
            }
        });
        if (check == 1) {
            alert('Already exist in compare list...');
            return;
        }
        var obj = newCompareProduct(mainObj.attr('prod_id'));
        var prod_info = {
            'prod_id': mainObj.attr('prod_id'),
            'prod_img': 'uploads/' + mainObj.attr('prod_img'),
            'prod_name': mainObj.attr('prod_name'),
            'obj': obj
        }
        compareList.push(prod_info);
        createCompierList();
    });

    $(document).on('focusin', 'input.compare-product-search', function () {
        var obj = $(this);
        $.each($('div.livesearch'), function () {
            $(this).css('display', 'none');
        });
        obj.parent().find('div.livesearch').css('display', 'block');
        return;
    });

    $(document).on('click', '.close_button', function () {
        removeItem($(this).parent().attr('prod_id'));
    });

    $(document).on('click', '.removeAll', function () {
        compareList = jQuery.grep(compareList, function (value) {
            value.obj.find('.compare').prop('checked', false);
        });
        createCompierList();
    });

    $(document).on('click', '.btn-compare', function (e) {
        e.preventDefault();
        var url = '?r=product/product-compare';
        var form = '<form action="' + url + '" method="post">';
        for (var i = 0; i < 4; i++) {
            if (i < compareList.length) {
                form = form + '<input type="text" name="product-' + i + '" value="' + compareList[i].prod_id + '" hidden="true">';
                continue;
            }
            form = form + '<input type="text" name="product-' + i + '" value="" hidden="true">';
        }
        form = form + '</form>';
        var fo = $(form);
        $('body').append(fo);
        fo.submit();

    });


    $(document).on('mouseenter', 'div.product-item', function () {
        $(this).find('.compare-div').css('display', 'block');
    }).on('mouseleave', 'div.product-item', function () {
        $(this).find('.compare-div').css('display', 'none');
    });


    var state = 0;
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() > $(document).height() - 300) {
            if (state == 1) {
                return;
            }
            state = 1;
            var brand = [];

            $.each($(document).find('.brand-name:checked'), function () {
                if ($(this).length > 0) {
                    brand.push($(this).val());
                }
            });

            var prod_att = [];
            $.each($(document).find('.prod_att:checked'), function () {
                if ($(this).length > 0) {
                    var value = {
                        'att_id': $(this).attr('att_id'),
                        'att_val': $(this).attr('att_val')
                    };
                    prod_att.push(value);
                }
            });

            var prod_id_list = $('div.product-item');
            var end_prod_id = $(prod_id_list[prod_id_list.length - 1]).attr('prod_id');

            var data = {
                'cat_id': $('#cat_id').val(),
                'sub_cat_id': $('#sub_cat_id').val(),
                'min_price': $('#min_price').val(),
                'max_price': $('#max_price').val(),
                'brand': brand,
                'prod_att': prod_att,
                'end_id': end_prod_id
            };


            $.post('?r=product/product-list-search', data, function (data) {
                $('.product-list-content').append(data);
                $.each($('div.product-item'), function () {
                    var obj = $(this);
                    $.each(compareList, function (key, value) {
                        if ($(obj).attr('prod_id') == value.prod_id) {
                            var ob = obj.find('div.compare-div>div.compare-sub-div>label.compare-label');
                            ob.find('.compare').prop('checked', true);
                            compareList[key].obj = ob;
                        }
                    });
                });
                state = 0;
            });

        }
    });


});