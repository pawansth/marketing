<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product_rating".
 *
 * @property int $id
 * @property int $prod_id
 * @property int $rating
 * @property string $rating_data
 * @property string $email
 * @property int $status
 *
 * @property Product $prod
 */
class ProductRating extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_rating';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prod_id', 'rating', 'rating_data', 'email'], 'required'],
            [['prod_id', 'rating', 'status'], 'integer'],
            [['rating_data'], 'safe'],
            [['email'], 'string', 'max' => 100],
            [['prod_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['prod_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prod_id' => 'Prod ID',
            'rating' => 'Rating',
            'rating_data' => 'Rating Data',
            'email' => 'Email',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProd()
    {
        return $this->hasOne(Product::className(), ['id' => 'prod_id']);
    }
}
