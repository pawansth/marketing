<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Product;

/**
 * ProductSearch represents the model behind the search form of `common\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['name', 'price', 'cat_id', 'sub_cat_id', 'brand_id', 'release_date', 'description'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find()->joinWith('cat')->joinWith('brand');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
//            'release_date' => $this->release_date,
//            'cat_id' => $this->cat_id,
//            'sub_cat_id' => $this->sub_cat_id,
//            'brand_id' => $this->brand_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'product.name', $this->name])
            ->andFilterWhere(['like', 'product.price', $this->price])
                ->andFilterWhere(['like', 'categories.name', $this->cat_id])
                ->andFilterWhere(['like', 'categories.name', $this->sub_cat_id])
                ->andFilterWhere(['like', 'brand.name', $this->brand_id])
                ->andFilterWhere(['like', 'product.release_date', $this->release_date])
                ->andFilterWhere(['like', 'product.description', $this->description]);
//        print_r($dataProvider); die();

        return $dataProvider;
    }
}
