<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'assets/bootstrap/css/bootstrap.min.css',
        'assets/metisMenu/metisMenu.min.css',
        'assets/dist/css/sb-admin-2.css',
        'assets/morrisjs/morris.css',
        'assets/font-awesome/css/font-awesome.min.css',
        'css/site.css',
    ];
    public $js = [
//        'assets/jquery/jquery.min.js',
        'assets/bootstrap/js/bootstrap.min.js',
        'assets/metisMenu/metisMenu.min.js',
        'assets/raphael/raphael.min.js',
        'assets/morrisjs/morris.min.js',
        'assets/data/morris-data.js',
        'assets/dist/js/sb-admin-2.js',
        'js/customScript.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
