<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;

use common\models\Categories;

/* @var $this yii\web\View */
/* @var $model common\models\Brand */
/* @var $form yii\widgets\ActiveForm */

$required = true;
$disabled = true;
?>

<div class="brand-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cat_id')->dropDownList(
            ArrayHelper::map(Categories::find()->where(['parent' => 0])->all(), 'id', 'name'),
            [
                'prompt' => 'SELECT MAIN CATEGORIES',
                'class' => 'form-control brand-main-categories',
                'onchange' => '$.post("index.php?r=categories/sub-cat-list&id="+$(this).val(),'
                    . 'function(data) {'
                    .'$("select.brand-sub-categories").html(data);'
                    . '$("select.brand-sub-categories").prop("disabled", false);'
                    .'$(document).find(".error-sbu").remove();'
                . '});',
                'required' => $required
            ]
            ) ?>

    <?= $form->field($model, 'sub_cat_id')->dropDownList(
            ArrayHelper::map(Categories::find()->where('parent != 0')->all(), 'id', 'name'),
            [
                'prompt' => 'SELECT SUB CATEGORIES',
                'disabled' => $disabled,
                'class' => 'form-control brand-sub-categories'
            ]
            ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
