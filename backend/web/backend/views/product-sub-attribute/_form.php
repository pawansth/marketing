<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\models\Categories;

use common\models\ProductAttribute;

/* @var $this yii\web\View */
/* @var $model common\models\ProductAttribute */
/* @var $form yii\widgets\ActiveForm */

$disabled = true;
$required = true;


if(empty($model->id)){
        $actionValue = Yii::$app->homeUrl.'?r=product-sub-attribute/create';
    } else {
        $actionValue = Yii::$app->homeUrl.'?r=product-sub-attribute/update&id='.$model->id;
//        $required = false;
//        $disabled = false;
    }
?>

<div class="product-attribute-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'cat_id')->dropDownList(
            ArrayHelper::map(Categories::find()->where(['parent'=>0])->all(), 'id', 'name'),
            [
                'prompt'   => 'SELECT CATEGORIES NAME',
                'class'    => 'form-control prod_sub_att_cat_id',
                'required' => $required
            ]
            ) ?>
    
    <?= $form->field($model, 'parent')->dropDownList(
            ArrayHelper::map(ProductAttribute::find()->where(['parent'=>0])->all(), 'id', 'name'),
            [
                'prompt'   => 'SELECT Parent NAME',
                'class'    => 'form-control prod_sub_att_parent',
                'disabled' => $disabled,
                'required' => $required
            ]
            ) ?>
    
    
    <div class="container-prod-sub-att-rows">
    <div class="row form-group row-prod-sub-att">
        <div class="col-md-10 pull-left">
            <?= $form->field($model, 'name')->textInput([
                'class' => 'form-control prod-sub-att-name',
                'maxlength' => true
            ])?>
        </div>
        <?php if(empty($model->id)) {?>
        <div class="col-md-1" style="margin-left: 0px; margin-right: 0px; padding-left: 0px; padding-right: 0px;">
            <?= Html::button('',[
                'class' => 'btn btn-info glyphicon glyphicon-plus  clone-prod-sub-att',
                'style' => 'border-radius: 50%; font-size: 15px; color: white;'
                . ' width: 25px; height: 25px; margin: 0px; margin-top: 25px;'
                . ' padding: 0px; text-align: center;'
                ]) ?>
        </div>
            <?php
            }
            ?>
    </div>
</div>
    
    <?php if(empty($model->id)) {?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success save-prod-sub-att-btn']) ?>
    </div>
    <?php
            } else {
            ?>
    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-success update-prod-sub-att-btn']) ?>
    </div>
    <?php 
            }
    ?>
    

    <?php ActiveForm::end(); ?>

</div>
