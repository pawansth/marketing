<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductAdvertiseCode */

$this->title = 'Create Product Association Link';
$this->params['breadcrumbs'][] = ['label' => 'Product Association Link', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-advertise-code-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
