<?php

namespace backend\controllers;

use Yii;
use common\models\Categories;
use backend\models\CategoriesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\provider\CategoriesProvider;

/**
 * CategoriesController implements the CRUD actions for Categories model.
 */
class CategoriesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
        ];
    }

    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $data = CategoriesProvider::categoriesList();
        return $this->render('list',['model' => $data]);
    }

    /**
     * Displays a single Categories model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Main Categories model.
     * If creation is successful, the browser will be redirected to the 'list' page.
     * @return mixed
     */
    public function actionAddMain()
    {
        $model = new Categories();

        if (Yii::$app->request->post()) {
            $datas = Yii::$app->request->post('data');
            $batchData = [];
            foreach ($datas as $value) {
                $batchData[] = [
                    'name' => $value['name'],
                    'link' => $value['link']
                    ];
//                $query = "insert into product_specification_topic(prod_spec_title_id, topics) values ('$product_spec_title_id', '$topic')";
            }
            \Yii::$app->db->createCommand()
                        ->batchInsert(
                                'categories',
                                ['name', 'link'],
                                $batchData
                                )
                        ->execute();
            return true;
        }

        return $this->render('addMain', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Categories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            print_r(Yii::$app->request->post());            return;
//            return $this->redirect(['index']);
        }

        if($model->parent == 0) {
            return $this->render('addMain', [
                'model' => $model,
            ]);
        } else {
            return $this->render('addSub', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Categories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionListSub() {
        $data = CategoriesProvider::categoriesList();
        return $this->render('listSub');
    }
    
    public function actionAddSub() {
        $model = new Categories();
            
        if (Yii::$app->request->post()) {
            $request = \Yii::$app->request;
            $datas = $request->post('data');
            
            $arrayOfValue = $datas[1]['value'];
            $batchData = [];
            foreach ($arrayOfValue as $value) {
                $batchData[] = [
                    'name' => $value['name'],
                    'parent' => $datas[0]['parent'],
                    'link' => $value['link']
                    ];
//                $query = "insert into product_specification_topic(prod_spec_title_id, topics) values ('$product_spec_title_id', '$topic')";
            }
            \Yii::$app->db->createCommand()
                        ->batchInsert(
                                'categories',
                                ['name', 'parent', 'link'],
                                $batchData
                                )
                        ->execute();
            return true;
        }

            return $this->render('addSub', [
                'model' => $model,
            ]);
    }

 public function actionUpdateSub($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('addSub', [
            'model' => $model,
        ]);
    }

    public function actionSubCatList($id) {
        
        $data = CategoriesProvider::categoriesList();
        foreach($data as $value) {
            if($value['id'] == $id) {
                return $this->recursive($value);
            }
        }
    }
    
    public function recursive($data) {
//        print_r($data[0]['node']); die();
//        echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
        if(!($data['parent'] == 0)) {
            if(sizeof($data) <= 7) {
                echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
            }
        } else {
            echo '<option value="'.$data['id'].'">SELECT SUB CATEGORIES</option>';
        }
        
        for($i = 0; $i < sizeof($data)-7; $i++) {
               $this->recursive($data[$i]['node']);
        }
    }

    /**
     * Finds the Categories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Categories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
