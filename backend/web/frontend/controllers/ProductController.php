<?php

namespace frontend\controllers;

use common\models\Brand;
use common\models\Product;
use common\models\ProductAttribute;
use common\models\ProductAttValue;

class ProductController extends \yii\web\Controller {

    public function beforeAction($action) {
        if (in_array($action->id, [
                    'product-list-search',
                    'product-compare',
                    'compare-product-search',
                    'product-search'
                ])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actions() {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionUpcoming($id) {

        $query = 'SELECT product.*, table1.rating, table2.path FROM product'
                . ' LEFT JOIN (SELECT SUM(rating) / COUNT(rating) AS rating,'
                . ' prod_id  FROM product_rating GROUP BY (prod_id)) table1'
                . ' ON product.id = table1.prod_id LEFT JOIN (SELECT * FROM'
                . ' product_img GROUP BY (prod_id)) table2 ON table2.prod_id'
                . ' = product.id WHERE product.sub_cat_id = ' . $id;
        $product = \Yii::$app->db->createCommand($query)
                ->queryAll();
//        print_r(\Yii::$app->db->createCommand($query)->getRawSql()); die();
//        return $this->render(
//                        'product-list', [
//                    'filters' => $this->productFilter($id),
//                    'products' => $product,
//                    'sub_cat_id' => $id,
//                        ]
//        );


        return $this->render(
                        'upcoming', [
                    'products' => $product,
                    'sub_cat_id' => $id,
                        ]
        );
    }

    public function productFilter($id = null) {
        $brand = Brand::find()->where(['sub_cat_id' => $id])->all();
        $query = 'SELECT product_attribute.id, product_attribute.name,'
                . ' product_attribute.cat_id, product_att_value.id as value_id,'
                . ' product_att_value.value FROM product_attribute'
                . ' RIGHT JOIN product_att_value'
                . ' ON product_attribute.id = product_att_value.prod_att_id'
                . ' WHERE product_attribute.cat_id = :categories_id'
                . ' ORDER BY product_attribute.name ASC';
        $productSpecification = \Yii::$app->db->createCommand($query)
                ->bindValue(':categories_id', ParentCategoriesProvider::parentCat($id))
                ->queryAll();

        return $this->renderPartial(
                        'product-filters', [
                    'brand' => $brand,
                    'productSpecification' => $productSpecification
                        ]
        );
    }

    public function actionProductCompare() {
        $request = \Yii::$app->request;
        if ($request->post()) {
            return $this->render(
                            'product-compare', [
                        'products' => $request->post(),
                            ]
            );
        }
        return $this->redirect(\Yii::$app->homeUrl);
    }

    public function actionProductSearch() {
        $request = \Yii::$app->request;
        /* Checking if parameter is pass along with request or not */
        if ($request->post()) {
            /* Accessing the parameters passed with request */
            $key_word = $request->post('key_word');
            /* Creating query for request */
            $query = 'SELECT product.id, product.name, product.brand_id, table2.path FROM product'
                    . ' LEFT JOIN (SELECT * FROM'
                    . ' product_img GROUP BY (prod_id)) table2 ON table2.prod_id'
                    . ' = product.id WHERE '
                    . ' product.name LIKE \'' . $key_word . '%\'';
            $product = \Yii::$app->db->createCommand($query) // executing the query
                    ->queryAll();
            /* Preparing to return value */
            $htmlReturn = '<ul class="list-group">';
            foreach ($product as $value) {
                $htmlReturn .= '<li class="list-group-item" prod_id="' . $value['id'] . '"'
                        . ' prod_img="' . $value['path'] . '" prod_name="' . $value['name'] . '">'
                        . '<a href="' . \Yii::$app->homeUrl . '?r=product/product-detail&id=' . $value['id'] . ''
                        . '&brandId='.$value['brand_id']
                        . '">'
                        . '<div class="row">'
                        . '<img width="30" height="80" '
                        . 'class="col-md-4" src="' . \Yii::getAlias('@web') . '/uploads/' . $value['path'] . '">'
                        . '<span class="col-md-8" style="'
                        . 'margin-top: 30px;'
                        . '">' . $value['name'] . '</span>'
                        . '</div>'
                        . '<a>'
                        . '</li>';
            }
            $htmlReturn .= '</ul>';

            return $htmlReturn;
        }
    }

    public function actionCompareProductSearch() {
        $request = \Yii::$app->request;
        /* Checking if parameter is pass along with request or not */
        if ($request->post()) {
            /* Accessing the parameters passed with request */
            $cat_id = $request->post('cat_id');
            $sub_cat_id = $request->post('sub_cat_id');
            $key_word = $request->post('key_word');
            /* Creating query for request */
            $query = 'SELECT product.id, product.name, table2.path FROM product'
                    . ' LEFT JOIN (SELECT * FROM'
                    . ' product_img GROUP BY (prod_id)) table2 ON table2.prod_id'
                    . ' = product.id WHERE product.cat_id = ' . $cat_id
                    . ' AND product.sub_cat_id = ' . $sub_cat_id
                    . ' AND product.name LIKE \'' . $key_word . '%\'';
            $product = \Yii::$app->db->createCommand($query) // executing the query
                    ->queryAll();
            /* Preparing to return value */
            $htmlReturn = '<ul class="list-group">';
            foreach ($product as $value) {
                $htmlReturn .= '<li class="list-group-item live-search-result" prod_id="' . $value['id'] . '"'
                        . ' prod_img="' . $value['path'] . '" prod_name="' . $value['name'] . '">'
                        . '<span>' . $value['name'] . '</span></li>';
            }
            $htmlReturn .= '</ul>';

            return $htmlReturn;
        }
    }

    public function actionProductList($id) {
        $query = 'SELECT product.*, table1.rating, table2.path FROM product'
                . ' LEFT JOIN (SELECT SUM(rating) / COUNT(rating) AS rating,'
                . ' prod_id  FROM product_rating GROUP BY (prod_id)) table1'
                . ' ON product.id = table1.prod_id LEFT JOIN (SELECT * FROM'
                . ' product_img GROUP BY (prod_id)) table2 ON table2.prod_id'
                . ' = product.id WHERE product.sub_cat_id = :id LIMIT 10';
        $product = \Yii::$app->db->createCommand($query)
                ->bindValue(':id', $id)
                ->queryAll();
        return $this->render(
                        'product-list', [
                    'filters' => $this->productFilter($id),
                    'products' => $product,
                    'sub_cat_id' => $id,
                        ]
        );
    }

    public function actionProductListSearch() {
        $request = \Yii::$app->request;
        if ($request->post()) {

            $cat_id = $request->post('cat_id');
            $sub_cat_id = $request->post('sub_cat_id');
            $min_price = $request->post('min_price');
            $max_price = $request->post('max_price');

            $brand = $request->post('brand');
            $prod_att = $request->post('prod_att');
            $end_id = $request->post('end_id');

            /*
             * Query to find product list according different conditon of search
             */
            $query = 'SELECT DISTINCT product.*, table1.rating, table2.path FROM product'
                    . ' LEFT JOIN (SELECT SUM(rating) / COUNT(rating) AS rating,'
                    . ' prod_id  FROM product_rating GROUP BY (prod_id)) table1'
                    . ' ON product.id = table1.prod_id LEFT JOIN (SELECT * FROM'
                    . ' product_img GROUP BY (prod_id)) table2 ON table2.prod_id'
                    . ' = product.id';

            if (sizeof($prod_att) > 0) {
                $query = $query . ' LEFT JOIN(SELECT * FROM product_att_value) table3'
                        . ' ON table3.prod_id = product.id';
            }

            $query = $query . ' WHERE product.id > ' . $end_id
                    . ' AND product.cat_id = ' . $cat_id . ' AND'
                    . ' product.sub_cat_id = ' . $sub_cat_id . ''
                    . ' And product.price >= ' . $min_price;

            if ($max_price < 50000) {
                $query = $query . ' AND product.price <= ' . $max_price;
            }

            /*
             * Managing query to search according brand and price
             *              */

            if (sizeof($brand) > 0) {
                $query = $query . ' AND (';
                foreach ($brand as $key => $value) {
                    if ($key == 0) {
                        $query = $query . " product.brand_id = " . $value;
                        continue;
                    }
                    $query = $query . " OR product.brand_id = " . $value;
                }
                $query = $query . ' )';
            }
            /*
             * Managing query to search according brand, price and specification
             *              */
            if (sizeof($prod_att) > 0) {
                $query = $query . ' AND (';
                foreach ($prod_att as $key => $value) {
                    if ($key == 0) {
                        $query = $query . " (table3.prod_att_id = " . $value['att_id'] .
                                " AND table3.value LIKE " . "'" . $value['att_val'] . "')";
                        continue;
                    }
                    $query = $query . " OR (table3.prod_att_id = " . $value['att_id'] .
                            " AND table3.value LIKE " . "'" . $value['att_val'] . "')";
                }
                $query = $query . ')';
            }

            $query = $query . ' LIMIT 10';

            // Creating db query using yii2 creat command
            $productList = \Yii::$app->db->createCommand($query);
//                    ->bindValue(':cat_id', $cat_id)
//                    ->bindValue(':sub_cat_id', $sub_cat_id)
//                    ->bindValue(':min_price', $min_price);
//            if($max_price < 50000) {
//                $productList->bindValue(':max_price', $max_price);
//            }
//            
//            if(sizeof($brand) > 0) {
//                foreach($brand as $key => $value) {
//                    $productList->bindValue(":brand_$key", $value);
//                }
//            }
//            print_r($productList->getRawSql());
            $result = $productList->queryAll();

            if (empty($result) && $end_id > 0) {
                return;
            }
            /*
             * Returning the partial html data
             */
            return $this->renderPartial(
                            'product-list-search', [
                        'products' => $result,
                        'cat_id' => $cat_id,
                        'sub_cat_id' => $sub_cat_id,
                            ]
            );
        }
    }

    public function actionProductDetail($id, $brandId) {
        $query = 'SELECT product.*, table1.rating, table2.path, table3.brand_name FROM product'
                . ' LEFT JOIN (SELECT SUM(rating) / COUNT(rating) AS rating,'
                . ' prod_id  FROM product_rating GROUP BY (prod_id)) table1'
                . ' ON product.id = table1.prod_id LEFT JOIN (SELECT * FROM'
                . ' product_img GROUP BY (prod_id)) table2 ON table2.prod_id'
                . ' = product.id LEFT JOIN (SELECT name AS brand_name, sub_cat_id FROM brand '
                . 'WHERE brand.id = :brand_id) table3 ON table3.sub_cat_id = product.sub_cat_id'
                . ' WHERE product.id = :id';
        $product = \Yii::$app->db->createCommand($query)
                ->bindValue(':id', $id)
                ->bindValue(':brand_id', $brandId)
                ->queryAll();
        $prodAdvCode = \common\models\ProductAdvertiseCode::find()
                ->where([
                    'prod_id' => $id
                ])
                ->asArray()
                ->all();

        $query = 'SELECT product_attribute.id, product_attribute.name,'
                . ' product_attribute.cat_id, product_attribute.parent, product_att_value.id as value_id,'
                . ' product_att_value.value FROM product_attribute'
                . ' RIGHT JOIN product_att_value'
                . ' ON product_attribute.id = product_att_value.prod_att_id'
                . ' WHERE product_att_value.prod_id = :id'
                . ' ORDER BY product_attribute.name ASC';
        $productSpecification = \Yii::$app->db->createCommand($query)
                ->bindValue(':id', $id)
                ->queryAll();
//        $relatedProduct = Product::find()
//                ->where([
//                    'cat_id' => $product[0]['cat_id'],
//                    'sub_cat_id' => $product[0]['sub_cat_id'],
//                ])
//                ->asArray()
//                ->all();

        $query = 'SELECT product.*, table1.rating, table2.path, table3.brand_name FROM product'
                . ' LEFT JOIN (SELECT SUM(rating) / COUNT(rating) AS rating,'
                . ' prod_id  FROM product_rating GROUP BY (prod_id)) table1'
                . ' ON product.id = table1.prod_id LEFT JOIN (SELECT * FROM'
                . ' product_img GROUP BY (prod_id)) table2 ON table2.prod_id'
                . ' = product.id LEFT JOIN (SELECT name AS brand_name, sub_cat_id FROM brand '
                . 'WHERE brand.id = :brand_id) table3 ON table3.sub_cat_id = product.sub_cat_id'
                . ' WHERE product.cat_id = :id AND product.sub_cat_id = :sub_id';
        $relatedProduct = \Yii::$app->db->createCommand($query)
                ->bindValue(':id', $product[0]['cat_id'])
                ->bindValue(':brand_id', $brandId)
                ->bindValue(':sub_id', $product[0]['sub_cat_id'])
                ->queryAll();
        return $this->render(
                        'product-detail', [
                    'productDetail' => $product,
                    'prodAdvCode' => $prodAdvCode,
                    'productSpecification' => $productSpecification,
                    'relatedProduct' => $relatedProduct,
                        ]
        );
    }

    public function actionProductReview($id) {
        $model = new \common\models\ProductReview();
        if (\Yii::$app->request->post()) {
            $model->load(\Yii::$app->request->post());
            $model->review_date = date('Y-m-d');
            if ($model->save()) {
                return $this->redirect([
                            'product-detail',
                            'id' => $model->prod_id,
                            'brandId' => Product::find()->where(['id' => $model->prod_id])->one()->brand_id
                ]);
            }
        }
        return $this->render(
                        'product-review', [
                    'model' => $model,
                    'id' => $id
                        ]
        );
    }

    public function actionProductRate($id) {
        $model = new \common\models\ProductRating();
        if (\Yii::$app->request->post()) {
            $model->load(\Yii::$app->request->post());
            $model->rating_data = date('Y-m-d');
            if ($model->save()) {
                return $this->redirect([
                            'product-detail',
                            'id' => $model->prod_id,
                            'brandId' => Product::find()->where(['id' => $model->prod_id])->one()->brand_id
                ]);
            }
        }
        return $this->render(
                        'product-rate', [
                    'model' => $model,
                    'id' => $id
                        ]
        );
    }

}
