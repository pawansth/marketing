<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss(
        ".site-contact {"
        . "background: #FFFFFF;"
        . "padding-top: 20px;"
        . "padding-bottom: 20px;"
        . "}"
        . ".parent-container {"
        . "width: 100%;"
        . "height: 100vh;"
        . "}"
        . ".contact-form {"
        . "width: 50%;"
        . "height 100%;"
        . "margin-left: auto;"
        . "margin-right: auto;"
        . "}"
);
?>
<div class="site-contact">

    <div class="parent-container">

        <div class="contact-form">

            <h1><?= Html::encode($this->title) ?></h1>

            <p>
                If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.
            </p>

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'message')->textarea(['rows' => 6, 'maxlength' => true]) ?>

            <?= $form->field($model, 'status')->checkbox(['value' => 'advertisement']) ?>

            <?= $form->field($model, 'captcha')->widget(Captcha::className()) ?>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>

</div>
