<?php
/* @var $this \yii\web\View */
/* @var $content string */

use common\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php
$path = "http://localhost/allgadgetreview-admin/web/";
Yii::setAlias('@mlone1', $path . 'image/footer-image');
Yii::setAlias('@mlone2', $path . 'image/header-image');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body id="style-1" style="background: #f5f5f5;">
        <?php $this->beginBody() ?>
        <!-- HEADER -->
        <header>
            <!-- top Header -->
            <div class="container-fluid">
                <div id="top-header">
                    <div class="container-fluid">
                        <?php
//              $headerdetail = \app\models\Header::find()->where(['status'=>1])->all();
//              foreach ($headerdetail as $i => $header) {  
                        ?>
                        <div class="pull-left">
                            <span><?php //  $header->title       ?></span>
                        </div>
                        <?php // }?>
                        <div class="pull-right">
                            <ul class="header-top-links">
                                <?php
//              $footerdetail = \app\models\Footer::find()->where(['status'=>1])->all();
//              foreach ($footerdetail as $i => $footer) {  
                                ?>
                                <li><a href="#"><?php // $footer->email       ?></a></li>
                                <li><a href="#">Phone No:<?php // $footer->phone_number      ?></a></li>
                                <?php // } ?>
                                <li><a href="#">FAQ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /top Header -->

            <!-- header -->
            <div class="container-fluid">
                <div id="header">

                    <div class="pull-left">
                        <!-- Logo -->
                        <div class="header-logo">
                            <a class="logo" href="<?= \yii\helpers\Url::to(['site/index']) ?>">
                                <?php
//              $headerdetail = \app\models\Header::find()->where(['status'=>1])->all();
//              foreach ($headerdetail as $i => $header) {  
                                ?>
                                <img src="<?php //  Yii::getAlias('@mlone2') . "/$header->header"       ?>" alt="">
                                <?php // } ?>
                            </a>
                        </div>
                        <!-- /Logo -->



                    </div>
                    <div class="pull-right">

                        <ul class="header-btns">
                            <!-- Account -->
                            <!-- <li class="header-account dropdown default-dropdown">
                                <div class="dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="true">
                                    <div class="header-btns-icon">
                                        <i class="fa fa-user-o"></i>
                                    </div>
                                    <strong class="text-uppercase">My Account <i class="fa fa-caret-down"></i></strong>
                                </div>
                                <a href="#" class="text-uppercase">Login</a> / <a href="#" class="text-uppercase">Join</a>
                                <ul class="custom-menu">
                                    <li><a href="#"><i class="fa fa-user-o"></i> My Account</a></li>
                                    <li><a href="#"><i class="fa fa-heart-o"></i> My Wishlist</a></li>
                                    <li><a href="#"><i class="fa fa-exchange"></i> Compare</a></li>
                                    <li><a href="#"><i class="fa fa-check"></i> Checkout</a></li>
                                    <li><a href="#"><i class="fa fa-unlock-alt"></i> Login</a></li>
                                    <li><a href="#"><i class="fa fa-user-plus"></i> Create An Account</a></li>
                                </ul>
                            </li> -->
                            <!-- /Account -->

                            <!-- Mobile nav toggle-->
                            <li class="nav-toggle">
                                <button class="nav-toggle-btn main-btn icon-btn"><i class="fa fa-bars"></i></button>
                            </li>
                            <!-- / Mobile nav toggle -->
                        </ul>
                    </div>
                </div>
                <!-- header -->
            </div>
            <!-- container -->
        </header>
        <!-- /HEADER -->
        <div  id="not-for-main" class="container-fluid">
            <!-- NAVIGATION -->
            <div id="navigation">
                <!-- container -->
                <div class="container-fluid">
                    <div id="responsive-nav">
                        <!-- category nav -->
                        <div class="category-nav show-on-click">
                            <span class="category-header">Categories <i class="fa fa-list"></i></span>
                            <ul class="category-list">
                                <?php
                                $categories = common\provider\CategoriesProvider::categoriesList();
                                foreach ($categories as $cat) {
                                    ?>

                                    <?php if ((sizeof($cat) <= 7)) { ?>
                                        <li>
                                            <?php if (!empty($cat['link'])) { ?>
                                                <a href="<?= $cat['link'] ?>">
                                                    <?= $cat['name'] ?>
                                                </a>
                                            <?php } else {
                                                ?>
                                                <a href="<?= Yii::$app->homeUrl ?>?r=product/product-list&id=<?= $cat['id'] ?>">
                                                    <?= $cat['name'] ?>
                                                </a>
                                            <?php } ?>
                                        </li>
                                    <?php } else { ?>
                                        <li class="dropdown side-dropdown main-categories">
                                            <a>
                                                <?= $cat['name'] ?>
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                            <?php
                                            recursive($cat);
                                        }
                                        ?>
                                    </li>

                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                        <!-- /category nav -->

                        <?php

                        function recursive($data) {
                            $forTest = 0;
                            if ((sizeof($data) <= 7) && (!($data['parent'] == 0))) {
                                ?>
                                <li>
                                    <?php if (!empty($cat['link'])) { ?>
                                        <a href="<?= $data['link'] ?>">
                                            <?= $data['name'] ?>
                                        </a>
                                    <?php } else { ?>
                                        <a href="<?= Yii::$app->homeUrl ?>?r=product/product-list&id=<?= $data['id'] ?>">
                                            <?= $data['name'] ?>
                                        </a>
                                    <?php }
                                    ?>
                                </li>
                                <?php
                            } else {
                                if (!($data['parent'] == 0)) {
                                    $forTest = 1;
                                    ?>
                                    <li class="dropdown side-dropdown">
                                        <a>
                                            <?= $data['name']; ?>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    <?php }
                                    ?>
                                    <?php
                                }
                                if (sizeof($data) > 7) {
                                    ?>
                                    <div class="custom-menu">
                                        <div class="row">
                                            <ul class="list-links">
                                                <?php
                                                for ($i = 0; $i < sizeof($data) - 7; $i++) {
                                                    recursive($data[$i]['node']);
                                                }
                                                ?>
                                            </ul>
                                        </div></div>
                                    <?php
                                    if ($forTest) {
                                        echo '</li>';
                                    }
                                    ?>
                                    <?php
                                }
                            }
                            ?>

                            <!-- menu nav -->
                            <div class="menu-nav">
                                <span class="menu-header">Menu <i class="fa fa-bars"></i></span>
                                <ul class="menu-list">
                                    <li><a href="<?= \yii\helpers\Url::to(['site/index']) ?>">Home</a></li>
                                    <li class="dropdown"><a class="dropdown-toggle upcoming-main" data-toggle="dropdown" aria-expanded="true">Upcoming <i class="fa fa-caret-down"></i></a>
                                        <div class="custom-menu" style="height: 50vh; overflow-x: auto;">
                                            <ul class="list-links">
                                                <?php
                                                $categories = common\provider\CategoriesProvider::categoriesList();
                                                foreach ($categories as $cat) {
                                                    if ((sizeof($cat) <= 7)) {
                                                        createDropDownMenu($cat['id'], $cat['name']);
                                                        continue;
                                                    }
                                                    recursivess($cat);
                                                }

                                                function recursivess($data) {
                                                    if ((sizeof($data) <= 7) && (!($data['parent'] == 0))) {
                                                        createDropDownMenu($data['id'], $data['name']);
                                                    }
                                                    for ($i = 0; $i < sizeof($data) - 7; $i++) {
                                                        recursivess($data[$i]['node']);
                                                    }
                                                }

                                                function createDropDownMenu($id, $name) {
                                                    ?>
                                                    <!--<li>-->
                                                    <a class="drop-down-item" href="<?= Yii::$app->homeUrl ?>?r=product/upcoming&id=<?= $id ?>">
                                                        <li><?= $name ?></li>
                                                    </a>
                                                    <hr>
                                                    <!--</li>-->
                                                <?php }
                                                ?>
                                                <?php
                                                $this->registerCss(
                                                        ".drop-down-item:hover {"
                                                        . "cursor: pointer;"
                                                        . "margin: 0;"
                                                        . "padding: 0;  "
                                                        . "}"
                                                        . ".upcoming-main:hover {"
                                                        . "cursor: pointer;"
                                                        . "}"
                                                );
                                                ?>
                                            </ul>

                                        </div>
                                    </li>
                                    <li><a href="<?= \yii\helpers\Url::to(['viewproduct/smartphones']) ?>">Smartphones</a></li>
                                    <li><a href="<?= \yii\helpers\Url::to(['site/about']) ?>">About</a></li>
                                    <li><a href="<?= \yii\helpers\Url::to(['site/contact']) ?>">contact</a></li>
                                    <!-- Search -->
                                    <div class="header-search pull-right" aria-expanded="true">
                                        <form>
                                            <input 
                                                class="input search-input" 
                                                type="text" placeholder="Search products..." 
                                                style="
                                                  background-image: url('css/search-icon-png-18.png');
                                                  background-position: 8px 10px;
                                                  background-repeat: no-repeat;
                                                  padding-left: 28px;
                                                  width: 100%;
                                                  " autocomplete="off">
<!--                                             <select class="input search-categories">
                                                <option value="0">All Categories</option>
                                                <option value="1">Category 01</option>
                                                <option value="1">Category 02</option>
                                            </select> -->
                                            <div class="livesearchproduct" style="
                                                 height: 50vh;
                                                 border: 1px solid #C5BEBC;
                                                 width: 100%;
                                                 overflow-x: auto;
                                                 display: none;
                                                 position: absolute;
                                                 z-index: 100;
                                                 box-shadow: 2px 2px #C5BEBC;
                                                 "></div>
                                        </form>
                                    </div>
                                    <!-- /Search -->
                                </ul>

                            </div>
                            
                            <!-- menu nav -->
                    </div>
                </div>
                <!-- /container -->
            </div>
        </div>
        <!-- /NAVIGATION -->


        <div class="container-fluid">
            <?=
            Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ])
            ?>
            <?= Alert::widget() ?>
            <noscript>
            <div class="jumbotron" style="background: red; color: white; margin: 10px;">
                You don't have javascript enabled.  The site would not work properly.
            </div>
            </noscript>
            <?= $content ?>
        </div>

        <!-- FOOTER -->
        <footer id="footer" class="section section-grey">
            <!-- container -->
            <div class="container-fluid">
                <!-- row -->
                <div class="row">
                    <!-- footer widget -->
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="footer">
                            <?php
//              $footerdetail = \app\models\Footer::find()->where(['status'=>1])->all();
//              foreach ($footerdetail as $i => $footer) {  
                            ?>
                            <!-- footer logo -->
                            <div class="footer-logo">
                                <a class="logo" href="#">
                                    <img src="<?php // Yii::getAlias('@mlone1') . "/$footer->footer_image"       ?>" alt="">
                                </a>
                            </div>
                            <!-- /footer logo -->

                            <p><?php // $footer->footer_descrip       ?></p>
                            <label><?php // $footer->email       ?></label><br>
                            <label><?php // $footer->location       ?></label>

                            <!-- footer social -->
                            <ul class="footer-social">
                                <li><a href="<?php // $footer->social_link1       ?>"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="<?php // $footer->socail_link2     ?>"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="<?php // $footer->social_link3       ?>"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="<?php // $footer->social_link4       ?>"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="<?php // $footer->social_link5       ?>"><i class="fa fa-pinterest"></i></a></li>
                            </ul>

                            <!-- /footer social -->
                        </div>
                        <?php //}   ?>
                    </div>
                    <!-- /footer widget -->

                    <!-- footer widget -->
                    <!--                    <div class="col-md-3 col-sm-6 col-xs-6">
                                            <div class="footer">
                                                <h3 class="footer-header">My Account</h3>
                                                <ul class="list-links">
                    
                                                    <li><a href="#">Compare</a></li>
                    
                                                </ul>
                                            </div>
                                        </div>-->
                    <!-- /footer widget -->

                    <div class="clearfix visible-sm visible-xs"></div>

                    <!-- footer widget -->
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="footer">
                            <h3 class="footer-header">Customer Service</h3>
                            <ul class="list-links">
                                <li><a href="#">About Us</a></li>
                                <!--<li><a href="#">FAQ</a></li>-->
                            </ul>
                        </div>
                    </div>
                    <!-- /footer widget -->

                    <!-- footer subscribe -->
                    <!--                    <div class="col-md-3 col-sm-6 col-xs-6">
                                            <div class="footer">
                                                <h3 class="footer-header">Stay Connected</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                                                <form>
                                                    <div class="form-group">
                                                        <input class="input" placeholder="Enter Email Address">
                                                    </div>
                                                    <button class="primary-btn">Join Newslatter</button>
                                                </form>
                                            </div>
                                        </div>-->
                    <!-- /footer subscribe -->
                </div>
                <!-- /row -->
                <hr>
                <!-- row -->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        <!-- footer copyright -->
                        <div class="footer-copyright">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Developed By:- <i class="fa fa-heart-o" aria-hidden="true"></i> <a href="https://www.facebook.com/anil.shrestha.9406417" target="_blank">Pawan Shrestha</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </div>
                        <!-- /footer copyright -->
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </footer>
        <!-- /FOOTER -->

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
<?php
$this->registerCssFile('@web/css/site.css');
?>