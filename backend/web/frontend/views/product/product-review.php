<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\captcha\Captcha;

use common\models\Product;

/* @var $this yii\web\View */
/* @var $model common\models\ProductReview */
/* @var $form yii\widgets\ActiveForm */

$this->title = Product::find()->where(['id' => $id])->one()->name;
?>


<div class="product-review-form" style="
     margin-left: 25%;
     margin-right: 25%;
     margin-top: 30px;
     padding: 30px;
     background: #FFFFFF;
     ">
    <div style="border-bottom: 1px solid #C6B7B3; font-size: 18px; font-weight: bold;">
        <?= Product::find()->where(['id' => $id])->one()->name ?>
    </div>

    <?php $form = ActiveForm::begin(); ?>

    <?=
    $form->field($model, 'prod_id')->hiddenInput([
        'value' => $id,
    ])->label('')
    ?>

    <?=
    $form->field($model, 'review')->textarea([
        'maxlength' => true,
        'rows' => 5
    ])
    ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    
     <?= $form->field($model, 'captcha')->widget(Captcha::className()) ?>


    <div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
