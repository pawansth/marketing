<?php

namespace backend\controllers;

use Yii;
use common\models\ProductAttValue;
use backend\models\ProductAttValueSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductAttValueController implements the CRUD actions for ProductAttValue model.
 */


use common\models\Product;
use common\models\ProductAttribute;

class ProductAttValueController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductAttValue models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductAttValueSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductAttValue model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductAttValue model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductAttValue();

        if (Yii::$app->request->post()) {
            $datas = Yii::$app->request->post('data');
//            print_r($datas); die();
            $batchData = [];
            foreach ($datas as $value) {
                $batchData[] = [
                    'prod_att_id' => $value['prod_att_id'],
                    'value' => $value['value'],
                    'prod_id' => $value['prod_id']
                    ];
            }
            \Yii::$app->db->createCommand()
                        ->batchInsert(
                                'product_att_value',
                                [
                                    'prod_att_id',
                                    'value',
                                    'prod_id'
                                ],
                                $batchData
                                )
                        ->execute();
            return true;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProductAttValue model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProductAttValue model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    
    public function actionAttHeaderList($id) {
        $cat_id = Product::find()->where("id = $id")->one()->cat_id;
        $prod_att_header = ProductAttribute::find()->where("cat_id = $cat_id && parent = 0")->all();
        
        foreach($prod_att_header as $key) {
            echo '<option value="'.$key['id'].'">'.$key['name'].'</option>';
        }
    }
    
    public function actionAttList($id) {
        $prod_att = ProductAttribute::find()->where("parent = $id")->all();
        foreach($prod_att as $key) {
            echo '<option value="'.$key['id'].'">'.$key['name'].'</option>';
        }
    }

    /**
     * Finds the ProductAttValue model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductAttValue the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductAttValue::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
