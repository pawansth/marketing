<?php

namespace backend\controllers;

use Yii;
use common\models\Product;
use backend\models\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\provider\CategoriesProvider;
use common\models\Categories;
use common\models\Brand;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if (Yii::$app->request->post()) {
            $datas = Yii::$app->request->post('data');
//            print_r($datas); die();
            $batchData = [];
            foreach ($datas as $value) {
                $batchData[] = [
                    'name' => $value['name'],
                    'price' => $value['price'],
                    'description' => $value['description'],
                    'release_date' => $value['release_date'],
                    'cat_id' => $value['cat_id'],
                    'sub_cat_id' => $value['sub_cat_id'],
                    'brand_id' => $value['brand_id']
                    ];
            }
            \Yii::$app->db->createCommand()
                        ->batchInsert(
                                'product',
                                [
                                    'name',
                                    'price',
                                    'description',
                                    'release_date',
                                    'cat_id',
                                    'sub_cat_id',
                                    'brand_id'
                                ],
                                $batchData
                                )
                        ->execute();
            return true;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionSubCatList($id) {
        $data = CategoriesProvider::categoriesList();
        foreach($data as $value) {
            if($value['id'] == $id) {
                return $this->recursive($value);
            }
        }
    }
    
    public function recursive($data) {
        if($data['parent'] == 0) {
            echo '<option value="'.$data['id'].'">Select sub categories...</option>';
        }
        if(!($data['parent'] == 0)) {
            if(sizeof($data) <= 7) {
                echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
            }
        }
        for($i = 0; $i < sizeof($data)-7; $i++) {
               $this->recursive($data[$i]['node']);
        }
    }
    
    public function actionBrandList($id) {
        $data = Brand::find()->where(['cat_id' => $id])->all();
        foreach ($data as $value) {
            echo '<option value="'.$value->id.'">'.$value->name.'</option>';
        }
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
