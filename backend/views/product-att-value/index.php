<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductAttValueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Specification Values';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-att-value-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product Specification Value', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'prod_att_id',
                'value' => 'prodAtt.name'
            ],
            'value',
            [
                'attribute' => 'prod_id',
                'value' => 'prod.name'
            ],
//            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
