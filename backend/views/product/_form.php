<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use common\models\Categories;
use common\models\Brand;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
$actionValue = '';
$required = true;
$disabled = true;

if (empty($model->id)) {
    $actionValue = Yii::$app->homeUrl . '?r=product/create';
} else {
    $actionValue = Yii::$app->homeUrl . '?r=product/update&id=' . $model->id;
//        $required = false;
//        $disabled = false;
}
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
    $form->field($model, 'cat_id')->dropDownList(
            ArrayHelper::map(Categories::find()->where(['parent' => 0])->all(), 'id', 'name'), [
        'prompt' => 'Select categories....',
        'class' => 'form-control prod_cat_id',
        'onchange' => 'var id = $(this).val();$.post(\'index.php?r=product/sub-cat-list&id=\'+id,'
        . 'function(data) {'
        . '$.post(\'index.php?r=product/brand-list&id=\'+id,'
        . 'function(data) {'
        . '$("select.prod-brand").html(data);'
        . '$("select.prod-brand").prop("disabled", false);'
        . '$(document).find(".error-sbu").remove();'
        . '});'
        . '$("select.prrod-sub-categories").html(data);'
        . '$("select.prrod-sub-categories").prop("disabled", false);'
        . '$(document).find(".error-sbu").remove();'
        . '});',
        'required' => $required
            ]
    )
    ?>

    <?=
    $form->field($model, 'sub_cat_id')->dropDownList(
            ArrayHelper::map(Categories::find()->where('parent != 0')->all(), 'id', 'name'), [
        'prompt' => 'Select sub categories....',
        'class' => 'form-control prrod-sub-categories',
        'disabled' => $disabled
            ]
    )
    ?>

    <?=
    $form->field($model, 'brand_id')->dropDownList(
            ArrayHelper::map(Brand::find()->all(), 'id', 'name'), [
        'prompt' => 'Select brand....',
        'class' => 'form-control prod-brand',
        'disabled' => $disabled,
        'required' => $required
            ]
    )
    ?>


    <div class="container-prod-rows">
        <div class="row form-group row-prod">
            <div class="col-md-10 pull-left">
                <?=
                $form->field($model, 'name')->textInput([
                    'maxlength' => true,
                    'class' => 'form-control prod-name',
                    'onfocusout' => '$(this).parent().find(\'.error\').remove()'
                ])
                ?>

                <?=
                $form->field($model, 'price')->textInput([
                    'maxlength' => true,
                    'class' => 'form-control prod-price',
                    'onfocusout' => '$(this).parent().find(\'.error\').remove()'
                ])
                ?>
                <div class="form-group">
                    <?= Html::label('Release Date') ?>
                    <?=
                    DatePicker::widget([
                        'name' => 'Product[release_date]',
                        'id' => 'product-release_date',
                        'value' => date('Y-m-d'),
                        'options' => [
                            'placeholder' => 'Select release date ...',
                            'class' => 'form-control prod-release-date'
                        ],
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true
                        ]
                    ]);
                    ?>
                </div>
                <?=
                        $form->field($model, 'description')
                        ->widget(\yii\redactor\widgets\Redactor::className(), [
                            'options' => [
                                'class' => 'form-control product-description'
                            ],
                            'clientOptions' => [
                                'imageManagerJson' => ['/redactor/upload/image-json'],
                                'imageUpload' => ['/redactor/upload/image'],
                                'fileUpload' => ['/redactor/upload/file'],
                                'plugins' => ['clips', 'fontcolor', 'imagemanager']
                            ],
                        ])
                ?>

            </div>
            <?php if (empty($model->id)) { ?>
                <div class="col-md-1" style="margin-left: 0px; margin-right: 0px; padding-left: 0px; padding-right: 0px;">
                    <?=
                    Html::button('', [
                        'class' => 'btn btn-info glyphicon glyphicon-plus  clone-prod',
                        'style' => 'border-radius: 50%; font-size: 15px; color: white;'
                        . ' width: 25px; height: 25px; margin: 0px; margin-top: 25px;'
                        . ' padding: 0px; text-align: center;'
                    ])
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
    </div>



    <?php if (empty($model->id)) { ?>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success save-prod-btn']) ?>
        </div>
        <?php
    } else {
        ?>
        <div class="form-group">
            <?= Html::submitButton('Update', ['class' => 'btn btn-success update-prod-btn']) ?>
        </div>
        <?php
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>
