<?php

use yii\helpers\Html;
use yii\grid\GridView;

use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'name',
            'price',
            [
                'attribute' => 'release_date',
                'value' => 'release_date',
//                'format' => 'raw',
                'filter' => DatePicker::widget([
                                'name' => 'ProductSearch[release_date]',
                                'id' => 'product-release_date',
                                'removeButton' => false,
                                'options' => [
                                    'placeholder' => 'Select release date ...',
                        '           class' => 'form-control prod-release-date'
                                ],
                                'pluginOptions' => [
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight' => true
                                ]
                            ])
            ],
            'description',
            [
                'attribute' => 'cat_id',
                'value' => 'cat.name'
            ],
            [
                'attribute' => 'sub_cat_id',
                'value' => 'subCat.name'
            ],
            [
                'attribute' => 'brand_id',
                'value' => 'brand.name'
            ],
            //'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
